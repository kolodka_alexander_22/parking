<?php

namespace App\Http\Controllers\Parking;

use App\Http\Controllers\Controller;
use App\Models\Parking\Car;
use App\Models\Parking\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;

class ParkingController extends Controller
{
    /**
     * Display start page
     *
     * @return \Illuminate\Http\Response
     */
    public function start()
    {
        return view('Parking.start');
    }

    public static function parkCars(Request $request)
    {
        {
            $cars = DB::table('customers')
                ->join('cars', 'cars.customer_id', '=', 'customers.id')
                ->where('cars.is_parked', '=', 0)
                ->get()->toArray();

            if(empty($cars)){
                return view('Parking.park_cars');
            }

            $customers = array();

            foreach ($cars as $car) {
                $car = (array)$car;
                if (!isset($customers[$car['customer_id']])) {
                    foreach (Customer::getFields() as $customerField) {
                        $customers[$car['customer_id']][$customerField] = $car[$customerField];
                    }
                    $customers[$car['customer_id']]['id'] = $car['customer_id'];
                    $customers[$car['customer_id']]['cars'] = array();
                }

                foreach (Car::getFields() as $carField) {
                    $customers[$car['customer_id']]['cars'][$car['id']][$carField] = $car[$carField];
                }
            }

            $customers = array_map(
                function ($customer) {
                    $customer['cars'] = array_map(
                        function ($car) {
                            return (object)$car;
                        },
                        $customer['cars']
                    );
                    return (object)$customer;
                },
                $customers
            );

            return view('Parking.park_cars', compact('customers'));
        }
    }

    public static function parkCarsUpdate(Request $request)
    {
        $customerId = $request->input('customer');
        $carIdList = $request->input('cars');

        if(empty($customerId)){
            return back()->withErrors(['msg' => 'Не заполнено id клиента']);
        }

        if(empty($carIdList)){
            return back()->withErrors(['msg' => 'Не выбраны автомобили']);
        }

        $doesCustomerExist = DB::table('customers')
            ->where('id', $customerId)
            ->count() === 1;

        if(!$doesCustomerExist){
            return back()->withErrors(['msg' => sprintf('Клиент с id %d не найден', $customerId)]);
        }

        $doCarsExist = DB::table('cars')
                ->whereIn('id', $carIdList)
                ->count() === count($carIdList);

        if(!$doCarsExist){
            return back()->withErrors(['msg' => 'Не все автомобили существуют']);
        }

        DB::table('cars')
            ->whereIn('id', $carIdList)
            ->update(array('is_parked' => 1));

        return back();
    }
}
