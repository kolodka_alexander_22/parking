<?php

namespace App\Http\Controllers\Parking;

use App\Http\Controllers\Controller;
use App\Models\Parking\Car;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;

class CarController extends Controller
{
    const ITEMS_PER_PAGE = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = DB::table('customers')
            ->join('cars', 'cars.customer_id', '=', 'customers.id')
            ->paginate(self::ITEMS_PER_PAGE);

        return view('Parking.cars', compact('paginator'));
    }

    /**
     * Display a listing of the parked cars.
     *
     * @return \Illuminate\Http\Response
     */
    public function parkedCars()
    {
        $paginator = DB::table('cars')
            ->where('is_parked', '=', 1)
            ->addSelect(DB::raw('COUNT(*) as car_count'))
            ->addSelect(array('marque', 'model'))
            ->groupBy(array('marque', 'model'))
            ->paginate(self::ITEMS_PER_PAGE);

        return view('Parking.parked_cars', compact('paginator'));
    }

    /**
     * Update the specified resources in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $cars = $request->input('cars');

        foreach ($cars as $index => $car) {
            $cars[$index]['is_parked'] = (isset($car['is_parked']) && $car['is_parked'] === 'on') ? 1 : 0;
        }

        $errors = new MessageBag();
        foreach ($cars as $car) {
            $errors->merge(Car::validate($car));
        }

        if ($errors->any()) {
            return back()->withErrors(['msg' => $errors->all()])
                ->withInput($request->input());
        }

        $newCars = array();

        foreach ($cars as $car) {
            if (preg_match('/n[0-9]+/', $car['id'])) {
                unset($car['id']);
                $newCars[] = $car;
                continue;
            }

            $carId = $car['id'];
            unset($car['id']);

            DB::table('cars')
                ->where('id', $carId)
                ->update($car);
        }

        if (!empty($newCars)) {
            DB::table('cars')
                ->insert($newCars);
        }

        return back()->withInput($request->input());
    }

    /**
     * Set is_parked = 1
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getIn($id)
    {
        $errors = Car::updateIsParked($id, 1);
        return back()->withErrors(['msg' => $errors->all()]);
    }

    /**
     * Set is_parked = 0
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getOut($id)
    {
        $errors = Car::updateIsParked($id, 0);
        return back()->withErrors(['msg' => $errors->all()]);
    }
}
