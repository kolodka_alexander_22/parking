<?php

namespace App\Http\Controllers\Parking;

use App\Http\Controllers\Controller;
use App\Models\Parking\Car;
use App\Models\Parking\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CustomerController extends Controller
{
    const ITEMS_PER_PAGE = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = DB::table('customers')
            ->join('cars', 'cars.customer_id', '=', 'customers.id')
            ->addSelect(DB::raw('COUNT(*) as car_count'))
            ->addSelect('customers.*')
            ->groupBy('customers.id')
            ->paginate(self::ITEMS_PER_PAGE);


        return view('Parking.customers', compact('paginator'));
    }

    /**
     * Show the form for creating a new customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Parking.customer_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = $request->input('customer');
        $cars = $request->input('cars');
        $customer['phone'] = preg_replace('/[^0-9]/', '', $customer['phone']);

        $errors = new MessageBag();

        if(empty($cars) || count($cars) < 1){
            $errors->add('msg', 'У клиента должна быть хотя бы одна машина');
            $customer = (object)$customer;
            return view('Parking.customer_create', compact('customer', 'errors'));
        }

        $errors->merge(Customer::validate($customer));

        $carNumbers = array_column($cars, 'car_number');

        if (count($carNumbers) != count(array_unique($carNumbers))) {
            $errors->add('msg', 'Номера машин должны быть уникальны');
        }

        foreach ($cars as $index => $car) {
            $cars[$index]['is_parked'] = (isset($car['is_parked']) && $car['is_parked'] === 'on') ? 1 : 0;
        }

        foreach ($cars as $car) {
            $errors->merge(Car::validate($car));
        }

        if ($errors->any()) {
            $customer = (object)$customer;
            $customer->cars = array_map(
                function ($car) {
                    return (object)$car;
                },
                $cars
            );

            return view('Parking.customer_create', compact('customer', 'errors'));
        }

        $id = DB::table('customers')
            ->insertGetId($customer);

        foreach ($cars as $index => $car) {
            $cars[$index]['customer_id'] = $id;
            unset($cars[$index]['id']);
        }

        unset($customer['id']);
        DB::table('cars')
            ->insert($cars);

        return redirect(route('parking.customers.edit', array($id)));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = DB::table('customers')
            ->where('id', $id)
            ->first();

        if (!$customer) {
            throw new NotFoundHttpException();
        }

        $cars = DB::table('cars')
            ->where('customer_id', $id)
            ->get();
        $customer->cars = $cars;
        return view('Parking.customer_edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = $request->input('customer');
        $customer['phone'] = preg_replace('/[^0-9]/', '', $customer['phone']);

        $errors = Customer::validate($customer);

        if ($errors->any()) {
            return back()->withErrors(['msg' => $errors->all()]);
        }

        unset($customer['id']);

        DB::table('customers')
            ->where('id', $id)
            ->update($customer);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = DB::table('customers')
            ->where('id', $id)
            ->first();

        if (!$customer) {
            throw new NotFoundHttpException();
        }

        DB::table('customers')
            ->where('id', $id)
            ->delete();

        return redirect(route('parking.customers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @param int $carId
     * @return \Illuminate\Http\Response
     */
    public function destroyCar(Request $request, $id, $carId)
    {
        $car = DB::table('cars')
            ->where('id', $carId)
            ->first();

        if (!$car || $car->customer_id != $id) {
            throw new NotFoundHttpException();
        }

        $countCars = DB::table('cars')
            ->where('customer_id', $id)
            ->count();

        if ($countCars <= 1) {
            DB::table('customers')
                ->where('id', $id)
                ->delete();
        }

        DB::table('cars')
            ->where('id', $carId)
            ->delete();

        return back();
    }
}
