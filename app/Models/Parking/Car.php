<?php

namespace App\Models\Parking;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;

class Car extends Model
{
    public static function validate($car)
    {
        $errors = new MessageBag();

        //Проверить наличие лишних полей
        if (!empty(array_diff(array_keys($car), self::getFields()))) {
            return $errors->add('msg',
                sprintf('Присуствуют лишние поля у автомобиля %s', $car['car_number'])
            );
        }

        //Проверить наличие обязательных полей
        if (!empty(array_diff(self::getRequiredFields(), array_keys($car)))) {
            return $errors->add('msg',
                sprintf('Заполните обязательные поля у автомобиля %s', $car['car_number'])
            );
        }

        if (!in_array((int)$car['is_parked'], [0, 1])) {
            $errors->add('msg',
                sprintf('Поле %s соддержит недопустимое значение у автомобиля %s',
                    self::getFieldTitle('is_parked'),
                    $car['car_number'])
            );
        }

        if (!in_array(mb_strlen($car['car_number']), [8, 9])
            || !preg_match('/[а-яА-Я][0-9]{3}[а-яА-Я]{2}[0-9]{2,3}$/msiu', $car['car_number'])) {
            return $errors->add('msg', sprintf('Неправильно заполнен %s', self::getFieldTitle('car_number')));
        }

        if (isset($car['id']) && is_numeric($car['id']) && $car['id'] > 0) {
            $dbCar = DB::table('cars')
                ->where('id', $car['id'])
                ->first();
            if (!$dbCar) {
                return $errors->add('msg', sprintf('Автомобиль с id %d не найден', $car['id']));
            }

            if (($dbCar->car_number != $car['car_number'] && self::doesCarNumberExist($car['car_number']))) {
                return $errors->add('msg', sprintf('Автомобиль с номером %s уже существует', $car['car_number']));
            }

        } elseif (self::doesCarNumberExist($car['car_number'])) {
            return $errors->add('msg', sprintf('Автомобиль с номером %s уже существует', $car['car_number']));
        }

        return $errors;
    }

    public static function updateIsParked($id, int $isParked)
    {
        $errors = new MessageBag();

        $doesCarExist = DB::table('cars')
                ->where('id', $id)
                ->count() == 1;

        if (!$doesCarExist) {
            return $errors->add('msg', sprintf('Автомобиль с id %d не найден', $id));
        }

        DB::table('cars')
            ->where('id', $id)
            ->update(array('is_parked' => $isParked));

        return $errors;
    }

    public static function doesCarNumberExist($carNumber)
    {
        $doesCarNumberExist = DB::table('cars')
                ->where('car_number', $carNumber)
                ->count() > 0;

        return $doesCarNumberExist;
    }

    public static function getFieldTitles()
    {
        return array(
            'id' => 'ID',
            'marque' => 'марка',
            'model' => 'модель',
            'color' => 'цвет',
            'car_number' => 'гос. номер',
            'is_parked' => 'припаркована',
        );
    }

    public static function getFieldTitle($title)
    {
        return self::getFieldTitles()[$title];
    }

    public static function getFields()
    {
        return array(
            'id',
            'marque',
            'model',
            'color',
            'car_number',
            'customer_id',
            'is_parked',
        );
    }

    public static function getRequiredFields()
    {
        return array(
            'marque',
            'model',
            'color',
            'car_number',
            'customer_id',
            'is_parked',
        );
    }
}
