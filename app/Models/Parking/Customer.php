<?php

namespace App\Models\Parking;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;

class Customer extends Model
{
    public static function validate($customer)
    {
        $errors = new MessageBag();

        //Проверить наличие лишних полей
        if (!empty(array_diff(array_keys($customer), self::getFields()))) {
            return $errors->add('msg', 'Присуствуют лишние поля у клиента');
        }

        //Проверить наличие обязательных полей
        if (!empty(array_diff(self::getRequiredFields(), array_keys($customer)))) {
            return $errors->add('msg', 'Заполните обязательные поля у клиента');
        }

        foreach (array('surname', 'name', 'last_name') as $field) {
            if (mb_strlen($customer[$field]) < 3) {
                $errors->add('msg', sprintf('Поле %s соддержать минимум 3 символа', self::getFieldTitle($field)));
            }
        }

        if (!in_array((int)$customer['is_male'], [0, 1])) {
            $errors->add('msg', sprintf('Поле %s соддержит недопустимое значение', self::getFieldTitle('is_male')));
        }


        if (strlen($customer['phone']) != 11 && !preg_match('/[0-9]{11}/', $customer['phone'])) {
            return $errors->add('msg', sprintf('Неправильно заполнен %s', self::getFieldTitle('phone')));
        }

        if (isset($customer['id']) && is_numeric($customer['id']) && $customer['id'] > 0) {
            $dbCustomer = DB::table('customers')
                ->where('id', $customer['id'])
                ->first();

            if (!$dbCustomer) {
                return $errors->add('msg', sprintf('Клиент с id %d не найден', $customer['id']));
            }

            if ($dbCustomer->phone != $customer['phone'] && self::doesPhoneExist($customer['phone'])) {
                return $errors->add('msg', sprintf('Клиент с телефоном %s уже существует', $customer['phone']));
            }

        } elseif (self::doesPhoneExist($customer['phone'])) {
            return $errors->add('msg', sprintf('Клиент с телефоном %s уже существует', $customer['phone']));
        }

        return $errors;
    }

    public static function doesPhoneExist($phone)
    {
        $doesPhoneExist = DB::table('customers')
                ->where('phone', $phone)
                ->count() > 0;

        return $doesPhoneExist;
    }

    public static function getFieldTitles()
    {
        return array(
            'id' => 'ID',
            'surname' => 'фамилия',
            'name' => 'имя',
            'last_name' => 'отчество',
            'is_male' => 'мужчина',
            'phone' => 'телефон',
            'address' => 'адрес',
        );
    }

    public static function getFieldTitle($title)
    {
        return self::getFieldTitles()[$title];
    }

    public static function getFields()
    {
        return array(
            'id',
            'surname',
            'name',
            'last_name',
            'is_male',
            'phone',
            'address',
        );
    }

    public static function getRequiredFields()
    {
        return array(
            'surname',
            'name',
            'last_name',
            'is_male',
            'phone',
        );
    }
}
