@extends('Parking.layouts.app')

@section('content')
    <div class="container parking">
        <div class="row">
            <div class="col-12 text-center">
                <h1>Все клиенты</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-1">
                <a href="{{route('parking.start')}}" class="btn btn-primary">Главная</a>
            </div>
            <div class="col-2">
                <a href="{{route('parking.customers.create')}}" class="btn btn-success">Добавить клиента</a>
            </div>
        </div>

        <div class="row">
            <div class="col-12 customers">
                <table class="table table-bordered table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Имя</th>
                        <th scope="col">Пол</th>
                        <th scope="col">Количество автомобилей</th>
                        <th scope="col">Телефон</th>
                        <th scope="col">Адрес</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($paginator as $customer)
                        <tr>
                            <td>{{$customer->id}}</td>
                            <td>{{implode(' ', array($customer->surname, $customer->name, $customer->last_name))}}</td>
                            <td>{{($customer->is_male === 1) ? 'Мужской' : 'Женский'}}</td>
                            <td>{{$customer->car_count}}</td>
                            <td>{{$customer->phone}}</td>
                            <td>{{$customer->address}}</td>
                            <td class="action-cell">
                                <div>
                                    <a href="{{route('parking.customers.edit', array($customer->id))}}"
                                       class="glyphicon glyphicon-pencil edit-row"></a>
                                </div>
                            </td>
                            <td class="action-cell">
                                <div>
                                    <a href="{{route('parking.customers.destroy', array($customer->id))}}"
                                       class="glyphicon glyphicon-remove remove-row" data-action="delete"></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @if($paginator->total() > $paginator->count())
            <div class="row justify-content-center">
                <div class="col-12">
                    {{ $paginator->links() }}
                </div>
            </div>
        @endif
    </div>

@endsection
