<div class="shadow p-3 mb-5 bg-white rounded" id="car-{{$car->id ?? '#car_index#'}}">
    <div class="form-row mb-4">
        <div class="col">
            <label>Марка</label>
            <input type="text" class="form-control" name="cars[{{$car->id ?? '#car_index#'}}][marque]" id="marque"
                   required="required" value="{{ $car->marque ?? ''}}">
            <input type="hidden" class="form-control" name="cars[{{$car->id ?? '#car_index#'}}][id]" id="marque"
                   required="required" value="{{ $car->id ?? '#car_index#'}}">
            <input type="hidden" class="form-control" name="cars[{{$car->id ?? '#car_index#'}}][customer_id]" id="marque"
                   required="required" value="{{ $car->customer_id ?? $customer->id ?? ''}}">
        </div>
        <div class="col">
            <label for="model">Модель</label>
            <input type="text" class="form-control" name="cars[{{$car->id ?? '#car_index#'}}][model]" id="model"
                   required="required" value="{{ $car->model ?? ''}}">
        </div>
    </div>

    <div class="form-row mb-4">
        <div class="col">
            <label>Государственный номер</label>
            <input type="text" class="form-control" name="cars[{{$car->id ?? '#car_index#'}}][car_number]" id="car_number"
                   required="required" value="{{ $car->car_number ?? ''}}">
        </div>
        <div class="col">
            <label>Цвет</label>
            <input type="text" class="form-control" name="cars[{{$car->id ?? '#car_index#'}}][color]" id="color"
                   required="required" value="{{ $car->color ?? ''}}">
        </div>
    </div>

    <div class="form-group mb-4">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="cars[{{$car->id ?? '#car_index#'}}][is_parked]"
                   id="is_parked" {{(($car->is_parked ?? 0)  === 1) ? 'checked' : '' }}>
            <label class="form-check-label">
                Автомобиль на стоянке
            </label>
        </div>
    </div>

    <div class="container mb-4">
        <div class="row">
           <div class="col-3">
               @if (isset($car) && isset($car->id) && $car->id >= 1)
                   <a href="{{ route('parking.customers.destroy.car', array($car->customer_id, $car->id))}}"
                      class="btn btn-danger ml-4">Удалить</a>
               @else
                   <div class="btn btn-danger remove-car" data-car-id="#car_index#">Удалить</div>
               @endif
           </div>
        </div>
    </div>
</div>
