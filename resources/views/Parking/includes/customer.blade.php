<div class="shadow p-3 mb-5 bg-white rounded">
    <div class="form-row mb-4 mt-4">
        <div class="col">
            <label for="surname">Фамилия</label>
            <input type="text" class="form-control" name="customer[surname]" id="surname"
                   placeholder="Введите фамилию" value="{{$customer->surname ?? ''}}">
            <input type="hidden" class="form-control" name="customer[id]"
                   placeholder="Введите фамилию" value="{{$customer->id ?? ''}}">
        </div>
        <div class="col">
            <label for="name">Имя</label>
            <input type="text" class="form-control" name="customer[name]" id="name"
                   placeholder="Введите имя" value="{{$customer->name ?? ''}}">
        </div>
        <div class="col">
            <label for="last_name">Отчество</label>
            <input type="text" class="form-control" name="customer[last_name]" id="last_name"
                   placeholder="Введите отчество" value="{{$customer->last_name ?? ''}}">
        </div>
    </div>
    <div class="form-row mb-4">
        <legend class="col-form-label">Пол</legend>
        <div class="form-check-inline">
            <input class="form-check-input" type="radio" name="customer[is_male]" id="male" value="1"
                {{(($customer->is_male ?? 1) == 1) ? 'checked' : '' }}>
            <label class="form-check-label" for="male">
                Мужской
            </label>
        </div>
        <div class="form-check-inline">
            <input class="form-check-input" type="radio" name="customer[is_male]" id="female" value="0"
                   {{(($customer->is_male ?? 1) == 0) ? 'checked' : '' }}>
            <label class="form-check-label" for="female">
                Женский
            </label>
        </div>
    </div>
    <div class="form-row mb-4">
        <div class="col-4">
            <label for="phone">Телефон</label>
            <input type="text" class="form-control" id="phone" name="customer[phone]"
                   placeholder="Введите телефон" value="{{$customer->phone ?? ''}}">
        </div>
    </div>
    <div class="form-row mb-4">
        <div class="col-6">
            <label for="address">Адрес</label>
            <input type="text" class="form-control" name="customer[address]" id="address"
                   value="{{$customer->address ?? ''}}">
        </div>
    </div>
</div>
