<script>
    function counter() {
        let count = 0;

        return function () {
            return ++count;
        }
    }

    function addCar(){
        let carInputs = $('#car-content').html().replace(/#car_index#/g, 'n' + carCounter());
        $(carInputs).insertBefore($("#add-car"));
    }

    let carCounter = counter();

    $(document).ready(function () {

        $("#phone").mask("+7 (999) 999-99-99", {placeholder: "+7 (xxx) xx-xx-xx"});

        $('#add-car').click(addCar);

        $('#cars').on('click', '.remove-car', function () {
            let elementId = $(this).data('car-id');
            $('#car-' + elementId).remove();
        });
    });
</script>
