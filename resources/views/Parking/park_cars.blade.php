@extends('Parking.layouts.app')

@section('content')

    <div class="container parking">
        <div class="row mb-5">
            <div class="col-12 text-center">
                <h1>Добавить машину на автостоянку</h1>
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-1">
                <a href="{{route('parking.start')}}" class="btn btn-primary">Главная</a>
            </div>
            <div class="col-2">
                <a href="{{route('parking.cars.index')}}" class="btn btn-primary">Все автомобили</a>
            </div>
            <div class="col-3">
                <a href="{{route('parking.cars.parked')}}" class="btn btn-primary">Автомобили на стоянке</a>
            </div>
        </div>

        @if($errors->any())
            <div class="row">
                <div class="col-12 text-center">
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            {{$error}}
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-12">
                @if(isset($customers))
                    <form method="POST" action="{{ route('parking.park.cars.update')}}">
                        @method('PATCH')
                        @csrf

                        <div class="form-group">
                            <label for="customer">Клиенты</label>
                            <select class="form-control" id="customer-select" name="customer">
                                @php
                                    $allCustomers = $customers;
                                    $firstCustomer = array_shift($customers);
                                @endphp
                                <option value="{{$firstCustomer->id}}" selected="selected">
                                    {{implode(' ', array($firstCustomer->surname, $firstCustomer->name, $firstCustomer->last_name))}}
                                </option>
                                @foreach($customers as $customer)
                                    <option value="{{$customer->id}}">
                                        {{implode(' ', array($customer->surname, $customer->name, $customer->last_name))}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="customer">Автомобили</label>
                            <select class="form-control" id="cars-select" name="cars[]" multiple>
                                @foreach($firstCustomer->cars as $car)
                                    <option value="{{$car->id}}">
                                        {{implode(' ', array($car->marque, $car->model, $car->color, $car->car_number))}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </form>
                @else
                    <div class="alert alert-info" role="alert">
                        Все автомобили припаркованы
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection

@section('scripts')
   @if(isset($customers))
       <script>
           function updateCarsSelector(customerId) {
               $('#cars-select').empty();
               for (let carId in Customers[customerId]['cars']) {
                   if (!Customers[customerId]['cars'].hasOwnProperty(carId)) {
                       return;
                   }
                   let car = Customers[customerId]['cars'][carId];

                   addCarOption(car);
               }
           }

           function addCarOption(car) {
               let carOption = document.createElement('option');
               carOption.text = [car.marque, car.model, car.color, car.car_number].join(' ');
               carOption.value = car.id;

               $('#cars-select').append(carOption);
           }

           window.Customers = @json($allCustomers);

           $(document).ready(function () {
               $('#customer-select').change(function () {
                   let customerId = $(this).val();
                   updateCarsSelector(customerId);
               });
           });
       </script>
    @endif
@endsection
