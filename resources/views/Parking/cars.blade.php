@extends('Parking.layouts.app')

@section('content')
    <div class="container parking">
        <div class="row">
            <div class="col-12 text-center">
                <h1>Все автомобили</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-1">
                <a href="{{route('parking.start')}}" class="btn btn-primary">Главная</a>
            </div>
            <div class="col-3">
                <a href="{{route('parking.cars.parked')}}" class="btn btn-primary">Автомобили на стоянке</a>
            </div>
            <div class="col-4">
                <a href="{{route('parking.park.cars')}}" class="btn btn-primary">Добавить автомобили на стоянку</a>
            </div>
        </div>

        @if($errors->any())
            <div class="row">
                <div class="col-12 text-center">
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            {{$error}}
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-12 customers">
                <table class="table table-bordered table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">ID машины</th>
                        <th scope="col">Владелец</th>
                        <th scope="col">Авто</th>
                        <th scope="col">Номер</th>
                        <th scope="col">Припаркована</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($paginator as $car)
                        <tr>
                            <td>{{$car->id}}</td>
                            <td>{{implode(' ', array($car->surname, $car->name, $car->last_name))}}</td>
                            <td>{{implode(' ', array($car->marque, $car->model))}}</td>
                            <td>{{$car->car_number}}</td>
                            <td>{{$car->is_parked === 1 ? 'Да' : 'Нет' }}</td>
                            <td class="action-cell">
                                <div>
                                    <a href="{{route('parking.customers.edit', array($car->customer_id))}}"
                                       class="glyphicon glyphicon-pencil edit-row"></a>
                                </div>
                            </td>
                            <td class="action-cell">
                                <div>
                                    <a href="{{route('parking.customers.destroy.car', array($car->customer_id, $car->id))}}"
                                       class="glyphicon glyphicon-remove remove-row" data-action="delete"></a>
                                </div>
                            </td>
                            <td class="action-cell">
                                <div>
                                    @if($car->is_parked == 1)
                                        <a href="{{route('parking.cars.get_out', array($car->id))}}"
                                           class="glyphicon glyphicon glyphicon-download remove-row"
                                           data-action="delete"></a>
                                    @else
                                        <a href="{{route('parking.cars.get_in', array($car->id))}}"
                                           class="glyphicon glyphicon glyphicon-upload remove-row"
                                           data-action="delete"></a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @if($paginator->total() > $paginator->count())
            <div class="row justify-content-center">
                <div class="col-12">
                    {{ $paginator->links() }}
                </div>
            </div>
        @endif
    </div>

@endsection
