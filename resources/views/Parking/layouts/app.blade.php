<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Parking</title>
    <link rel="stylesheet" href="{{ mix('Parking/css/app.css') }}">
</head>
<body>

@yield('content')

<script src="{{ mix('Parking/js/libs.js') }}"></script>

@yield('scripts')

</body>
</html>
