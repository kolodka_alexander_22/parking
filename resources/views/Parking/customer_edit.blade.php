@extends('Parking.layouts.app')

@section('content')

    <div class="container parking">
        <div class="row">
            <div class="col-12 text-center">
                <h1>Клиент</h1>
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-1">
                <a href="{{route('parking.start')}}" class="btn btn-primary">Главная</a>
            </div>
            <div class="col-1">
                <a href="{{route('parking.customers.index')}}" role="button" class="btn btn-primary">Клиенты</a>
            </div>
        </div>

        @if($errors->any())
            <div class="row">
                <div class="col-12 text-center">
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            {{$error}}
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="row justify-content-center">

            <div class="col-12">
                <form method="POST" action="{{ route('parking.customers.update', array($customer->id)) }}">
                    @method('PATCH')
                    @csrf
                    @include('Parking.includes.customer')
                    <div class="container">
                        <div class="row mt-5 mb-5">
                            <div class="col-4">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                                <a href="{{ route('parking.customers.destroy', array($customer->id)) }}"
                                   class="btn btn-danger ml-4">Удалить</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row justify-content-center" id="cars">

            <div class="col-12">
                <form method="POST" action="{{ route('parking.cars.update', array($customer->id)) }}">
                    @method('PATCH')
                    @csrf
                    <div class="form-row mb-4">
                        <div class="col-6">
                            <h2>Автомобили</h2>
                            <div class="alert alert-warning" role="alert">
                                Укажите как минимум 1 автомобиль
                            </div>
                        </div>
                    </div>

                    @foreach ($customer->cars as $car)
                        @include('Parking.includes.car')
                    @endforeach

                    @php
                        unset($car);
                    @endphp

                    <div class="btn btn-primary" id="add-car">Добавить автомобиль</div>

                    <div class="container">
                        <div class="row mt-5 mb-5">
                            <div class="col-3">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="d-none" id="car-content">
                    @include('Parking.includes.car')
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @include('Parking.includes.parking_scripts')
@endsection
