@extends('Parking.layouts.app')

@section('content')
    <div class="main">
        <h1>Система учёта клиентов автостоянки</h1>
        <div class="container">
            <div class="row justify-content-center mt-5">
                <div class="col-1">
                    <a href="{{route('parking.customers.index')}}" role="button" class="btn btn-primary">Клиенты</a>
                </div>
                <div class="col-1">
                    <a href="{{route('parking.cars.index')}}" role="button" class="btn btn-primary">Автомобили</a>
                </div>
            </div>
        </div>
    </div>
@endsection
