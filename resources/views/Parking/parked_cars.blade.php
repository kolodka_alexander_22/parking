@extends('Parking.layouts.app')

@section('content')
    <div class="container parking">
        <div class="row">
            <div class="col-12 text-center">
                <h1>Автомобили на стоянке</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-1">
                <a href="{{route('parking.start')}}" class="btn btn-primary">Главная</a>
            </div>
            <div class="col-2">
                <a href="{{route('parking.cars.index')}}" class="btn btn-primary">Все автомобили</a>
            </div>
            <div class="col-4">
                <a href="{{route('parking.park.cars')}}" class="btn btn-primary">Добавить автомобили на стоянку</a>
            </div>
        </div>

        <div class="row">
            <div class="col-12 customers">
                <table class="table table-bordered table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Марка, модель</th>
                        <th scope="col">Количество припаркованых автомобилей</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($paginator as $car)
                        <tr>
                            <td>{{implode(' ', array($car->marque, $car->model))}}</td>
                            <td>{{$car->car_count}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @if($paginator->total() > $paginator->count())
            <div class="row justify-content-center">
                <div class="col-12">
                    {{ $paginator->links() }}
                </div>
            </div>
        @endif
    </div>

@endsection
