<?php

Route::group(['namespace' => 'Parking', 'prefix' => 'parking'], function (){
    Route::resource('customers', 'CustomerController', ['except'=> ['show']])->names('parking.customers');
    Route::get('/customers/{id}/destroy/', 'CustomerController@destroy')->name('parking.customers.destroy');
    Route::get('/customers/{id}/destroy/car/{car_id}', 'CustomerController@destroyCar')->name('parking.customers.destroy.car');
    Route::resource('cars', 'CarController')->only(['index', 'update'])->names('parking.cars');
    Route::get('/cars/parked/', 'CarController@parkedCars')->name('parking.cars.parked');
    Route::get('/park-cars/', 'ParkingController@parkCars')->name('parking.park.cars');
    Route::patch('/park-cars/', 'ParkingController@parkCarsUpdate')->name('parking.park.cars.update');
    Route::get('/cars/{id}/get_in', 'CarController@getIn')->name('parking.cars.get_in');
    Route::get('/cars/{id}/get_out', 'CarController@getOut')->name('parking.cars.get_out');
});

Route::get('', 'Parking\ParkingController@start')->name('parking.start');

