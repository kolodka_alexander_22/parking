<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = array();
        for($i = 0; $i <= 100; $i++){
            $customer = array();
            $customer['name'] = $this->getNames()[array_rand( $this->getNames())];
            $customer['surname'] = $this->getSurNames()[array_rand( $this->getSurNames())];
            $customer['last_name'] = $this->getLastNames()[array_rand( $this->getLastNames())];
            $customer['phone'] = (string) rand(11111111111, 99999999999);
            $customer['is_male'] = 1;
            $customer['address'] = 'Волгоград улица ' . $i;
            $customers[] = $customer;
        }

        DB::table('customers')->insert($customers);
    }

    public function getNames(){
        return array(
            'Пётр',
            'Николай',
            'Александр',
            'Алексей',
            'Игнат',
            'Фёдор',
            'Михаил',
        );
    }

    public function getSurNames(){
        return array(
            'Иванов',
            'Петров',
            'Берёзов',
            'Синицин',
            'Соловьёв',
            'Сергеев',
            'Сидоров',
        );
    }

    public function getLastNames(){
        return array(
            'Иванович',
            'Петрович',
            'Александрович',
            'Николаевич',
            'Петрович',
            'Сергеевич',
            'Михаилович',
        );
    }
}
