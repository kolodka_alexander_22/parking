<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cars = array();

        $customerIdList = DB::table('customers')->select('id')->get()->toArray();

        $customerIdList = array_map(
            function ($customerId) {
                return $customerId->id;
            },
            $customerIdList
        );

        for($i = 0; $i <= 1000; $i++){
            $car = array();
            $car['marque'] = $this->getMarques()[array_rand( $this->getMarques())];
            $car['model'] = $this->getModels()[array_rand( $this->getModels())];
            $car['color'] = $this->getColors()[array_rand( $this->getColors())];
            $car['car_number'] = 'т' . rand(111, 999) . 'тт' .rand(11, 999);
            $car['is_parked'] = rand(0, 1);
            $car['customer_id'] = $customerIdList[array_rand($customerIdList)];
            $cars[] = $car;
        }

        DB::table('cars')->insert($cars);
    }

    public function getMarques(){
        return array(
            'Lada',
            'Renault',
            'Audi',
            'Bentley',
            'BMW',
            'Opel',
            'Nissan',
        );
    }

    public function getModels(){
        return array(
            'X6',
            'Калина',
            'Duster',
            'Logan',
            'Granta'
        );
    }

    public function getColors(){
        return array(
            'Чёрный',
            'Белый',
            'Вишнёвый',
            'Красный',
            'Жёлтый',
            'Серый',
            'Зелёный',
        );
    }
}
